<head>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/jquery-1.11.1.min.js"></script>
    <script src="../resource/js/bootstrap.min.js"></script>
</head>

<?php



require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;

$objBookTitle  =  new BookTitle();
$objBookTitle->setData($_GET);
$oneData= $objBookTitle->viewSingle("obj");


?>


<table class="table table-striped">
    <thead>
    <tr>

        <th>ID</th>
        <th>Book Title</th>
        <th>Author</th>
    </tr>
    </thead>
    <tbody>
    <?php

        echo "<tr>";
        echo "<td>".$oneData->book_id."</td>";
        echo "<td>".$oneData->book_title."</td>";
        echo "<td>".$oneData->author_name."</td>";
        echo "</tr>";

    ?>
    </tbody>
</table>
