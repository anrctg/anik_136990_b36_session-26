
<head>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/jquery-1.11.1.min.js"></script>
    <script src="../resource/js/bootstrap.min.js"></script>
</head>

<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;
use App\Message\Message;


$objBookTitle = new BookTitle();

$allData = $objBookTitle->index("obj");

//print_r($allData);


$serialId = 1;
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Author</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($allData as $sigleData){
        echo "<tr>";
        echo "<td>".$serialId."</td>";
        echo "<td>".$sigleData->book_id."</td>";
        echo "<td>".$sigleData->book_title."</td>";
        echo "<td>".$sigleData->author_name."</td>";




        echo "<td>";

        echo "<a href='view.php?id=$sigleData->book_id'><button class='btn btn-info'>View</button></a> ";
        echo "<a href='edit.php?id=$sigleData->book_id'><button class='btn btn-primary'>Edit</button></a> ";
        echo "<a href='delete.php?id=$sigleData->book_id'><button class='btn btn-danger'>Delete</button></a> ";


        echo "</td>";
        echo "</tr>";

        $serialId++;
    }
    ?>
    </tbody>
</table>
